var newRowNum = 1;

$(document).ready(function(){
//	$(".add-button").click(function(){
//		var cloned = $(".firstQuestion").clone();
//		$(".firstQuestion").clone().appendTo(".questions");
////		$(".questions>div.hidden").removeClass("hidden");
//	});
});

function cloneDiv(rowNum) {
	$('#remove'+rowNum).show();
	newRowNum=rowNum+1;
	var cloned = $("#questionId"+rowNum).clone();
	cloned.appendTo(".questions");
	cloned.prop('id',"questionId" + newRowNum);
	updateRowNumbers(newRowNum,newRowNum);
	$('#add'+newRowNum).attr("onclick","cloneDiv("+newRowNum+")");
	$('#remove'+newRowNum).attr("onclick","removeDiv("+newRowNum+")");
	$("#hiddenQuestionId"+newRowNum).val(newRowNum);
	$('#add'+rowNum).hide();
};

function removeDiv(rowNum) {
	$('#questionId'+rowNum).remove();
	
	//This will make sure you can't use minus when only one question remains.
	var questionIdNodeList = document.getElementsByName("hiddenQuestionIds");
	if(questionIdNodeList.length==1){
		var firstRow = questionIdNodeList.item(0);
		$('#remove'+firstRow.value).hide();
	}
	
	//This will make sure the add button is not removed from the last question.
	var lastRow = questionIdNodeList[questionIdNodeList.length - 1];
	$('#add'+lastRow.value).show();
};

function updateRowNumbers(currentRow,newRowNum){
	$("#questionId"+currentRow).find("*").each(function() {
//    	  console.log("Current element: " + this.id);
    	  if(this.id != null && this.id != "" && this.id.match(/\d+/) != null){
    		  var rowNumInId = this.id.match(/\d+/)[0]; //locates the number in the id
//        	  console.log(rowNumInId);
        	  if(rowNumInId >= 0){
        		  //change row number in the id attribute
        		  this.id = this.id.replace(rowNumInId,newRowNum.toString());
//        		console.log("New element id: " + this.id);
        		  if(this.name){
        			//change row number in the name attribute
        			  this.name = this.name.replace(rowNumInId,newRowNum.toString());
//        			  console.log("New element name: " + this.name);
        		  }
        	  }
    	  }
    	  
  	});
	
	$("#coverage_"+currentRow).attr("id",'coverage_'+newRowNum);
};