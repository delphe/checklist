package com.nw.pgmchecklist

import grails.transaction.Transactional

@Transactional
class TemplateService {

    /**
     * Save all of your questions associated to a template.
     * 
     * @param templateInstance - pass in a saved template
     * @param params - pass in all of the questions
     * @return - nothing
     */
	def saveQuestions(templateInstance, params){
		params.hiddenQuestionIds.each{
			def question = new Question()
			question.inputType = params."questionType${it}"
			question.labelText = params."questionText${it}"
			question.template = templateInstance
			question.save(flush:true)
		}
	}
}
