import com.nw.pgmchecklist.Role
import com.nw.pgmchecklist.Theuser
import com.nw.pgmchecklist.TheuserRole

class BootStrap {

    def init = { servletContext ->

      def adminRole = new Role('ROLE_ADMIN').save()
      def userRole = new Role('ROLE_USER').save()

      def adminUser = new Theuser('admin', 'password').save()
	  def normalUser = new Theuser('user', 'password').save()

      TheuserRole.create adminUser, adminRole, true
	  TheuserRole.create normalUser, userRole, true

   }

    def destroy = {
    }
}
