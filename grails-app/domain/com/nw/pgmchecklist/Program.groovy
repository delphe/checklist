package com.nw.pgmchecklist

class Program {
	String programName
	String progress = "Started"
	Date dateCreated
	Template template
	Theuser theuser
	static hasMany = [answers: Answer]
	
	
    static constraints = {
		programName unique:true
		template nullable:true
		//answers nullable:true
		theuser nullable:false
    }
}
