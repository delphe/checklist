package com.nw.pgmchecklist

class Question {
	
	String inputType
	String labelText
	Template template

    static constraints = {
		inputType nullable:false
		labelText nullable:false
		template nullable:true
    }
}
