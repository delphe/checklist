package com.nw.pgmchecklist

import grails.gorm.DetachedCriteria
import groovy.transform.ToString

import org.apache.commons.lang.builder.HashCodeBuilder

@ToString(cache=true, includeNames=true, includePackage=false)
class TheuserRole implements Serializable {

	private static final long serialVersionUID = 1

	Theuser theuser
	Role role

	TheuserRole(Theuser u, Role r) {
		this()
		theuser = u
		role = r
	}

	@Override
	boolean equals(other) {
		if (!(other instanceof TheuserRole)) {
			return false
		}

		other.theuser?.id == theuser?.id && other.role?.id == role?.id
	}

	@Override
	int hashCode() {
		def builder = new HashCodeBuilder()
		if (theuser) builder.append(theuser.id)
		if (role) builder.append(role.id)
		builder.toHashCode()
	}

	static TheuserRole get(long theuserId, long roleId) {
		criteriaFor(theuserId, roleId).get()
	}

	static boolean exists(long theuserId, long roleId) {
		criteriaFor(theuserId, roleId).count()
	}

	private static DetachedCriteria criteriaFor(long theuserId, long roleId) {
		TheuserRole.where {
			theuser == Theuser.load(theuserId) &&
			role == Role.load(roleId)
		}
	}

	static TheuserRole create(Theuser theuser, Role role, boolean flush = false) {
		def instance = new TheuserRole(theuser: theuser, role: role)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(Theuser u, Role r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = TheuserRole.where { theuser == u && role == r }.deleteAll()

		if (flush) { TheuserRole.withSession { it.flush() } }

		rowCount
	}

	static void removeAll(Theuser u, boolean flush = false) {
		if (u == null) return

		TheuserRole.where { theuser == u }.deleteAll()

		if (flush) { TheuserRole.withSession { it.flush() } }
	}

	static void removeAll(Role r, boolean flush = false) {
		if (r == null) return

		TheuserRole.where { role == r }.deleteAll()

		if (flush) { TheuserRole.withSession { it.flush() } }
	}

	static constraints = {
		role validator: { Role r, TheuserRole ur ->
			if (ur.theuser == null || ur.theuser.id == null) return
			boolean existing = false
			TheuserRole.withNewSession {
				existing = TheuserRole.exists(ur.theuser.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['theuser', 'role']
		version false
	}
}
