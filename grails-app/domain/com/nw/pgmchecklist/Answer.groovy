package com.nw.pgmchecklist

class Answer {
	
	String question
	String inputValue
	Boolean answeredYes
	Date dateCreated
	Program program
	Theuser theuser
		

    static constraints = {
		question nullable:false
		inputValue nullable:true
		answeredYes nullable:true
		program nullable:true
		theuser nullable:false
    }
}

