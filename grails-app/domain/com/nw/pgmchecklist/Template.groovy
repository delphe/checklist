package com.nw.pgmchecklist

class Template {
	String templateName
	Theuser theuser
	static hasMany = [questions: Question]

    static constraints = {
		templateName unique:true
    }
}
