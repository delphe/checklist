package com.nw.pgmchecklist



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProgramController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def springSecurityService

    def index(Integer max) {
		def templates = Template.findAll()
		//TODO: Make it more responsive (goes for all pages)
		params.max = Math.min(max ?: 10, 100)
        respond Program.list(params), model:[programInstanceCount: Program.count(), templates:templates]
    }

    def show(Program programInstance) {
        respond programInstance
    }

    def create() {
		def template
		if(params.selectedTemplate){
			template = Template.findByTemplateName(params.selectedTemplate)
		}
		if(!template){
			template = Template.first()
		}
		
        respond new Program(params), model:[template: template]
    }

    @Transactional
    def save(Program programInstance) {
        if (programInstance == null) {
            notFound()
            return
        }

		def questionArray = []
		params.each{
			if(it.key.contains("questionId")){
				def question = Question.get(it.value)
				questionArray << question
			}
		}
		
		programInstance.theuser = springSecurityService.currentUser
		programInstance.validate()

		def template
		if(params.templateId){
			template = Template.get(params.templateId.toInteger())
		}
        if (programInstance.hasErrors()) {
            respond programInstance.errors, view:'create', model:[template: template]
            return
        }

        programInstance.save flush:true
		
		boolean answerHasError = false
		def answerArray = []
		questionArray.each{
			def answer = new Answer()
			answer.question = it.labelText
			def answeredYes = null
			if(params."answeredYes${it.id}" == "yes") {answeredYes=true}
			if(params."answeredYes${it.id}" == "no") {answeredYes=false}
			//TODO: if answeredYes is null and inputText is null error exists (check on gsp through javascript)
			answer.answeredYes = answeredYes
			answer.inputValue = params."inputValue${it.id}"
			answer.program = programInstance
			answer.theuser = springSecurityService.currentUser
			answer.save(flush:true)
		}

		flash.message = message(code: 'default.created.message', args: [message(code: 'program.label', default: 'Program'), programInstance.programName])
		redirect(action: "edit", id:programInstance.id)
		return
    }

    def edit(Program programInstance) {
        respond programInstance
    }

    @Transactional
    def update(Program programInstance) {
        if (programInstance == null) {
            notFound()
            return
        }
		
		def qnaArray = []
		params.each{
			if(it.key.contains("qnaId")){
				def answer = Answer.get(it.value)
				qnaArray << answer
			}
		}

        if (programInstance.hasErrors()) {
            respond programInstance.errors, view:'edit'
            return
        }

        programInstance.save flush:true

		qnaArray.each{
			def answer = it
			answer.question = params."question${it.id}"
			def answeredYes
			if(params."answeredYes${it.id}" == "yes") {answeredYes=true}
			if(params."answeredYes${it.id}" == "no") {answeredYes=false}
			answer.answeredYes = answeredYes
			answer.inputValue = params."inputValue${it.id}"
			answer.program = programInstance
			answer.theuser = springSecurityService.currentUser
//			println answer.hasErrors()
			answer.save(flush:true)
		}
		
		flash.message = message(code: 'default.updated.message', args: [message(code: 'Program.label', default: 'Program'), programInstance.programName])
		redirect action:"index", method:"GET"
		return
    }

    @Transactional
    def delete(Program programInstance) {

        if (programInstance == null) {
            notFound()
            return
        }

        programInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Program.label', default: 'Program'), programInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'program.label', default: 'Program'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
