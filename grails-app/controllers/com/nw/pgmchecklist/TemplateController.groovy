package com.nw.pgmchecklist



import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.SpringSecurityService
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TemplateController {
	
	def templateService
	def questionTypeMap = [text:"Text",radio:"Yes or No"]
	
	SpringSecurityService springSecurityService
    
	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Template.list(params), model:[templateInstanceCount: Template.count()]
    }
	
    def show(Template templateInstance) {
        respond templateInstance
    }

    def create() {
		//TODO: Make it pretty (bootstrap)
		//TODO: change add button to a plus sign and add a minus sign to remove
        respond new Template(params), model:[questionTypeMap:questionTypeMap]
    }

    @Transactional
    def save(Template templateInstance) {
		
        if (templateInstance == null) {
            notFound()
            return
        }
		
		templateInstance.theuser = springSecurityService.currentUser
		templateInstance.validate()
        if (templateInstance.hasErrors()) {
            respond templateInstance.errors, view:'create'
            return
        }
		
        templateInstance.save flush:true
		
		templateService.saveQuestions(templateInstance, params)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'template.label', default: 'Template'), templateInstance.id])
                redirect action:"index"
            }
            '*' { respond templateInstance, [status: CREATED] }
        }
    }

    def edit(Template templateInstance) {
		//TODO: Display all questions and allow similar functionality as create
		//TODO: Make sure all questions are displaying correctly
		
        respond templateInstance, model:[questions:templateInstance.questions, 
										questionTypeMap:questionTypeMap]
    }

    @Transactional
    def update(Template templateInstance) {
        if (templateInstance == null) {
            notFound()
            return
        }

        if (templateInstance.hasErrors()) {
            respond templateInstance.errors, view:'edit'
            return
        }

        templateInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Template.label', default: 'Template'), templateInstance.templateName])
                redirect action:"index"
            }
            '*'{ respond templateInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Template templateInstance) {
        if (templateInstance == null) {
            notFound()
            return
        }
		//TODO: Delete all questions first before deleting the instance. Or, use cascade delete.

        templateInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Template.label', default: 'Template'), templateInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'template.label', default: 'Template'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
