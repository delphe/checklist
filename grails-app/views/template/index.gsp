<%@ page import="com.nw.pgmchecklist.Template" %>
<%--<%@ page import="com.nw.pgmchecklist.Program" %>--%>
<!DOCTYPE html>
<html>
	<style>
		.tr .th .td .table .thead .tbody{
			box-sizing: border-box;
			display: table;
			border-collapse: collapse;
			position: relative;
			padding:0;
		}
	</style>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'template.label', default: 'Template')}" />
<%--		<g:set var="entityName2" value="${message(code: 'program.label', default: 'Program')}" />--%>
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<ul class="nav nav-tabs">
	<body>
		<div id="list-template" class="content scaffold-list bs-example snippet" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="alert alert-info" role="status">${flash.message}</div>
			</g:if>
			
			
				<ul class="nav nav-tabs">
				<li class="active">
				
				<a href="#1a" data-toggle="tab">Templates</a>
				</li>
				<li>
				<a href="#2a" data-toggle="tab">Review</a>
				</li>
				</ul>
				
				<div class="tab-content clearfix">
				<g:link class="create pull-right" action="create" style="font-size: 20px; text-align: center; width: 15%; background-color: #F47321; color: white; border-radius: 5px;">Create New</g:link>
				<br>
				<br>
				<div class="tab-pane active" id="1a">
				<table class="table table-hover">
				<thead>
					<tr class="table-col-boolean">
						<g:sortableColumn class="table-col-boolean" property="templateName" title="${message(code: 'template.templateName.label', default: 'Template Name')}" />
						<g:sortableColumn class="table-col-boolean" property="templateName" title="${message(code: 'template.templateName.label', default: 'Created By')}" />
						<g:sortableColumn class="table-col-boolean" property="templateName" title="${message(code: 'template.templateName.label', default: 'Options')}" />
					</tr>
				</thead>
				<tbody>
					<g:each in="${templateInstanceList}" status="i" var="templateInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
							<th class="table-col-boolean"><g:link action="edit" id="${templateInstance.id}">${fieldValue(bean: templateInstance, field: "templateName")}</g:link></th>
							<th class="table-col-boolean">${fieldValue(bean: templateInstance.theuser, field: "username")}</th>
	<%--						<th>--%>
	<%--							<g:link class="template-options" action="edit" id="${templateInstance.id}">Edit</g:link>--%>
	<%--						</th>--%>
							<th class="table-col-boolean">
								<g:form url="[resource:templateInstance, action:'delete']" method="DELETE">
									<fieldset>
										<g:actionSubmit action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
									</fieldset>
								</g:form>
							</th>
						</tr>
					</g:each>
				</tbody>
			</table>
			</div>	
			
			
					
			<div class="tab-pane" id="2a">
				<table class="table table-hover">
				<p>
				Work in progress
				</p>
<%--					<thead>--%>
<%--						<tr>--%>
<%--							<g:sortableColumn property="programName" title="${message(code: 'programName.label', default: 'Program Name')}" />--%>
<%--							<g:sortableColumn code="Program.theuser.label" default="Submitted By" />--%>
<%--							<g:sortableColumn property="dateCreated" title="${message(code: 'dateCreated.label', default: 'Date Submitted')}" />--%>
<%--						</tr>--%>
<%--					</thead>--%>
<%--					<tbody>--%>
<%--						<g:each in="${programInstanceList}" status="i" var="programInstance">--%>
<%--							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">--%>
<%--						--%>
<%--								<td><g:link action="program/index" id="${programInstance.id}">${fieldValue(bean: programInstance, field: "programName")}</g:link></td>--%>
<%--								<td>${fieldValue(bean: programInstance, field: "program.theuser.username")}</td>--%>
<%--								<td>${fieldValue(bean: programInstance, field: "program.dateCreated")}</td>--%>
<%--						--%>
<%--							</tr>--%>
<%--						</g:each>--%>
<%--					</tbody>--%>
				</table>
		
			</div>
			</div>
			</div>
			<div class="pagination">
				<g:paginate total="${templateInstanceCount ?: 0}" />
			</div>
	</body>
	</ul>
</html>
