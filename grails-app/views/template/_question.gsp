<g:set var="i" value="${i ?: 0}"/>

<div class="fieldcontain ${hasErrors(bean: questionInstance, field: 'inputType', 'error')} required">
	<label for="inputType${i}">
		<g:message code="question.inputType.label" default="Input Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="inputType${i}" id="inputType${i}" required="" value="${questionInstance?.inputType}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: questionInstance, field: 'labelText', 'error')} required">
	<label for="labelText">
		<g:message code="question.labelText.label" default="Label Text" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="labelText${i}" id="labelText${i}" required="" value="${questionInstance?.labelText}"/>

</div>
