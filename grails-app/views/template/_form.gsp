<%@ page import="com.nw.pgmchecklist.Template" %>
<g:javascript src="question-clone.js" />
<g:set var="i" value="${i ?: 0}"/>

<div class="fieldcontain ${hasErrors(bean: templateInstance, field: 'templateName', 'error')} required">
	
	<div class="row">
		<div class="form-group col-md-4">
			<label for="templateName">
				<g:message code="template.templateName.label" default="Template Name" />
				<span class="required-indicator">*</span>
			</label>
			<g:textField class="form-control" name="templateName" required="" value="${templateInstance?.templateName}"/>
		</div>
	</div>
	
	<g:if test="${!questions}">
		<g:render template="newRow"/>
	</g:if>
	<g:each status="i" in="${questions}" var="question">
		<g:render template="newRow" model="['question':question]"/>
	</g:each>
	
	<div class="questions"></div>


</div>
