<div id="questionId${i}">
	<div class="row">
		<div class="form-group col-md-2">
			<label>Question:</label>
			
			<g:select class="form-control" name="questionType${i}" from="${questionTypeMap?.entrySet()}" 
      				optionKey="key" optionValue="value" value="${question?.inputType}"/>
		</div>
		
		<div class="form-group col-md-8">
			<label>&nbsp;</label>
			<div class="input-group">
			<g:textField class="form-control" name="questionText${i}" id="questionText${i}" value="${question?.labelText}"/>
			
			<span class="input-group-btn">
				<button type="button" class="add-button btn btn-info" id="add${i}" onclick="cloneDiv(${i})">
					  <span class="icon icon-plus"></span>
					</button>
					<button type="button" class="minus-button btn btn-danger" id="remove${i}" style="display: none;" onclick="removeDiv(${i})">
					  <span class="icon icon-minus"></span>
					</button>
			</span>
			</div>
		</div>

	</div>
	<g:hiddenField name="hiddenQuestionIds" id="hiddenQuestionId${i}" value="${i}" />
	
</div>
