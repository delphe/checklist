<%@ page import="com.nw.pgmchecklist.Answer" %>



<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'question', 'error')} required">
	<label for="question">
		<g:message code="answer.question.label" default="Question" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="question" required="" value="${answerInstance?.question}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'inputValue', 'error')} ">
	<label for="inputValue">
		<g:message code="answer.inputValue.label" default="Input Value" />
		
	</label>
	<g:textField name="inputValue" value="${answerInstance?.inputValue}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'answeredYes', 'error')} ">
	<label for="answeredYes">
		<g:message code="answer.answeredYes.label" default="Answered Yes" />
		
	</label>
	<g:checkBox name="answeredYes" value="${answerInstance?.answeredYes}" />

</div>

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'program', 'error')} ">
	<label for="program">
		<g:message code="answer.program.label" default="Program" />
		
	</label>
	<g:select id="program" name="program.id" from="${com.nw.pgmchecklist.Program.list()}" optionKey="id" value="${answerInstance?.program?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: answerInstance, field: 'theuser', 'error')} required">
	<label for="theuser">
		<g:message code="answer.theuser.label" default="Theuser" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="theuser" name="theuser.id" from="${com.nw.pgmchecklist.Theuser.list()}" optionKey="id" required="" value="${answerInstance?.theuser?.id}" class="many-to-one"/>

</div>

