
<%@ page import="com.nw.pgmchecklist.Answer" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'answer.label', default: 'Answer')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-answer" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-answer" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list answer">
			
				<g:if test="${answerInstance?.question}">
				<li class="fieldcontain">
					<span id="question-label" class="property-label"><g:message code="answer.question.label" default="Question" /></span>
					
						<span class="property-value" aria-labelledby="question-label"><g:fieldValue bean="${answerInstance}" field="question"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${answerInstance?.inputValue}">
				<li class="fieldcontain">
					<span id="inputValue-label" class="property-label"><g:message code="answer.inputValue.label" default="Input Value" /></span>
					
						<span class="property-value" aria-labelledby="inputValue-label"><g:fieldValue bean="${answerInstance}" field="inputValue"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${answerInstance?.answeredYes}">
				<li class="fieldcontain">
					<span id="answeredYes-label" class="property-label"><g:message code="answer.answeredYes.label" default="Answered Yes" /></span>
					
						<span class="property-value" aria-labelledby="answeredYes-label"><g:formatBoolean boolean="${answerInstance?.answeredYes}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${answerInstance?.program}">
				<li class="fieldcontain">
					<span id="program-label" class="property-label"><g:message code="answer.program.label" default="Program" /></span>
					
						<span class="property-value" aria-labelledby="program-label"><g:link controller="program" action="show" id="${answerInstance?.program?.id}">${answerInstance?.program?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${answerInstance?.theuser}">
				<li class="fieldcontain">
					<span id="theuser-label" class="property-label"><g:message code="answer.theuser.label" default="Theuser" /></span>
					
						<span class="property-value" aria-labelledby="theuser-label"><g:link controller="theuser" action="show" id="${answerInstance?.theuser?.id}">${answerInstance?.theuser?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${answerInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="answer.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${answerInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:answerInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${answerInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
