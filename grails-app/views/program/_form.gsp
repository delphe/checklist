<%@ page import="com.nw.pgmchecklist.Program" %>


 <div class="row">
	<div class="form-group col-md-3 value ${hasErrors(bean: programInstance, field: 'programName', 'has-error')}">
		<label for="programName">Program Name</label>
		<g:textField class="form-control" name="programName" required="" value="${programInstance?.programName}"/>
	</div>
</div>
<g:hiddenField name="templateId" value="${template?.id}" />


<g:if test="${programInstance.answers}">
	<g:each status="i" var="qna" in="${programInstance.answers.sort()}">
		<g:hiddenField name="qnaId${qna?.id}" value="${qna.id}" />
		<g:hiddenField name="question${qna?.id}" value="${qna.question}" />
		
		<g:if test="${qna.answeredYes != null}">
			 <div class="btn-group" data-toggle="buttons" style="padding-bottom:10px;">
			 	<label> &nbsp; &nbsp;${qna.question}</label>
			        <label class="btn btn-info">
			        	<g:radio name="answeredYes${qna?.id}" id="answeredYes${qna?.id}" value="yes" checked="${qna.answeredYes}"/>Yes
			        </label>
			        <label class="btn btn-danger">
			        	<g:radio name="answeredYes${qna?.id}" id="answeredYes${qna?.id}" value="no" checked="${!qna.answeredYes}"/>No
			        </label>
			    </div>
			    <br/>
		</g:if>
		<g:if test="${(qna.question && !qna.inputValue && qna.answeredYes == null) || (qna.inputValue && qna.answeredYes == null)}">
			 <div class="row">
				<div class="form-group col-md-6 value">
					<label for="inputValue${qna?.id}">${qna.question}</label>
					<g:textField class="form-control" name="inputValue${qna?.id}" value="${qna?.inputValue}" />
				</div>
			</div>	
		</g:if>
	</g:each>
</g:if>
<g:else>
	<g:each status="i" var="question" in="${template?.questions?.sort()}">
		<g:hiddenField name="questionId${question?.id}" value="${question.id}" />
		
		<g:if test="${question.inputType == 'radio' }">
			 <div class="btn-group" data-toggle="buttons" style="padding-bottom:10px;">
			 	<label> &nbsp; &nbsp;${question.labelText}</label>
			        <label class="btn btn-info">
			        	<g:radio name="answeredYes${question?.id}" value="yes"/>Yes
			        </label>
			        <label class="btn btn-danger">
			        	<g:radio name="answeredYes${question?.id}" value="no"/>No
			        </label>
			    </div>
			    <br/>
		</g:if>
		<g:if test="${question.inputType == 'text' }">
			 <div class="row">
				<div class="form-group col-md-6 value">
					<label for="inputValue${question?.id}">${question.labelText}</label>
					<g:textField class="form-control" name="inputValue${question?.id}" value="${answer?.inputValue}" />
				</div>
			</div>	
		</g:if>
	</g:each>

</g:else>