<%@ page import="com.nw.pgmchecklist.Program" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'program.label', default: 'Program')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		
		<div id="edit-program" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="alert alert-info" role="status">${flash.message}</div>
			</g:if>
			<g:if test="${flash.error}">
			<div class="alert alert-error" role="status">${flash.error}</div>
			</g:if>
			<g:hasErrors bean="${programInstance}">
	            <div class="alert alert-error">
	            	<strong>Please Review And Confirm The Following Fields.</strong>
	                <g:renderErrors bean="${programInstance}" as="list" />
	            </div>
            </g:hasErrors>
			<g:form url="[resource:programInstance, action:'update']" method="PUT" >
				<g:hiddenField name="version" value="${programInstance?.version}" />
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
					<g:submitButton name="create" class="save" value="Complete" />
					<g:submitButton name="create" class="save" value="Send for Approval" style="width:200px;"/>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
