<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'program.label', default: 'Program')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		
		<div id="create-program" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			
			<g:hasErrors bean="${programInstance}">
	            <div class="alert alert-error">
	            	<strong>Please Review And Confirm The Following Fields.</strong>
	                <g:renderErrors bean="${programInstance}" as="list" />
	            </div>
            </g:hasErrors>
            <g:if test="${flash.error}">
				<div class="alert alert-error" role="status">${flash.error}</div>
			</g:if>
			
			<g:form url="[resource:programInstance, action:'save']" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="Save" />
					<g:submitButton name="create" class="save" value="Complete" />
					<g:submitButton name="create" class="save" value="Send for Approval" style="width:200px;"/>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
