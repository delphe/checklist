
<%@ page import="com.nw.pgmchecklist.Program" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'program.label', default: 'Program')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
		
			<g:if test="${flash.message}">
				<div class="alert alert-info" role="status">${flash.message}</div>
			</g:if>
			
			
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg create pull-right" data-toggle="modal" data-target="#myModal" style="font-size: 20px; text-align: center; width: 15%; background-color: #F47321; color: white; border-radius: 5px;">New Program</button>
 			
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Program Type</h4>
        </div>
        <div class="modal-body">
        	<g:each var="template" in="${templates}">
        		<p>
        			<g:link action="create" params="[selectedTemplate: template.templateName]">${template.templateName}</g:link>
        		</p>
			
        	</g:each>
        </div>

      </div>
      
    </div>
  </div>
			
			
			<table class="table table-hover">
			<thead>
					<tr>
					
						<g:sortableColumn property="programName" title="${message(code: 'program.programName.label', default: 'Program Name')}" />
					
<%--						<th><g:message code="program.template.label" default="Template" /></th>--%>
					
						<th><g:message code="program.theuser.label" default="Created By" /></th>
					
<%--						<g:sortableColumn property="dateCreated" title="${message(code: 'program.dateCreated.label', default: 'Date Created')}" />--%>
					
						<g:sortableColumn property="progress" title="${message(code: 'program.progress.label', default: 'Progress')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${programInstanceList}" status="i" var="programInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="edit" id="${programInstance.id}">${fieldValue(bean: programInstance, field: "programName")}</g:link></td>
<%--					--%>
<%--						<td>${fieldValue(bean: programInstance, field: "template")}</td>--%>
					
						<td>${fieldValue(bean: programInstance, field: "theuser.username")}</td>
					
					
<%--						<td><g:formatDate date="${programInstance.dateCreated}" /></td>--%>
					
						<td>${fieldValue(bean: programInstance, field: "progress")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${programInstanceCount ?: 0}" />
			</div>
		
	</body>
</html>
