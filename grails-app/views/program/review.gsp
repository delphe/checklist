
<%@ page import="com.nw.pgmchecklist.Program" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'program.label', default: 'Program')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-program" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-program" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:link class="create pull-left" action="template/index" value='Template' style="font-size: 20px; text-align: center; width: 15%; background-color: #005691; color: white; border-radius: 5px;">Template</g:link>
			<g:link class="create pull-left" action="program/review" value='Reveiw' style="font-size: 20px; text-align: center; width: 15%; background-color: #d8d8d8; color: white; border-radius: 5px;">Review</g:link>
			<g:link class="create pull-right" action="create" value='Create New' style="font-size: 20px; text-align: center; width: 15%; background-color: #F47321; color: white; border-radius: 5px;">Create New</g:link>
			<table class="table table-hover">
			<thead>
					<tr>
						<g:sortableColumn property="programName" title="${message(code: 'program.programName.label', default: 'Program Name')}" />
						<g:sortableColumn code="program.theuser.label" default="Submitted By" />
						<g:sortableColumn property="dateCreated" title="${message(code: 'program.dateCreated.label', default: 'Date Submitted')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${programInstanceList}" status="i" var="programInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="review" id="${programInstance.id}">${fieldValue(bean: programInstance, field: "programName")}</g:link></td>
						<td>${fieldValue(bean: programInstance, field: "theuser.username")}</td>
						<td>${fieldValue(bean: programInstance, field: "dateCreated")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${programInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
