<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'sveg.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/bootstrap-3.1', file: 'bootstrap.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/bootstrap-3.1', file: 'non-responsive.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/themes', file: 'scottsdale-theme.css')}" type="text/css">
		<link rel="stylesheet" href="${resource(dir: 'css/themes', file: 'ui-framework-theme.css')}" type="text/css">
		<%--	jquery needed to be renamed to include the version to prevent the jquery plugin from being used. --%>
		
		
		
		
		<g:javascript src="jquery-1.11.1.js" />
		
		<g:javascript src="bootstrap.js" />
		<g:javascript src="moment.js" />
		<g:javascript src="bootstrap-datetimepicker.js" />
		<g:javascript src="jquery.tablesorter.js" />
		<%--<g:javascript src="tablesorter.js" />--%>
		<g:javascript src="bootstrap-multiselect.js" />
		<g:javascript src="spin.js" />
		<g:javascript src="jquery-mask.js" />
		<g:javascript src="application.js" />
		<r:layoutResources />
<%--		<script src="https://use.fontawesome.com/76c5e95126.js"></script>--%>
		<g:layoutHead/>	
		
	</head>
	
	
	
	
	
	<body>
	<div class="navbar-fixed-top">
		<nav class="navbar navbar-inverse navbar-main" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
				 		<g:img dir="images" file="nw-logo-white.png" width="160" height="60" />
				 	</div>
				 	<div class="col-sm-6">
				 		<a class="nav_item nn_yeahh pull-right" href="${createLink(controller: 'logout')}"><h2>  <i class="fa fa-sign-out" aria-hidden="true"></i>Logout</h2> </a>
				 		<a class="nav_item nn_yeahh" href="${createLink(uri: '/')}"><h2><i class="fa fa-home" aria-hidden="true"></i> Home  </h2></a>
				 		<g:if test="${1 == 1}">
     						<a class="nav_item nn_yeahh" href="${createLink(uri:'/template/index.gsp')}"><h2><i class="fa fa-glass" aria-hidden="true"></i> Admin  </h2></a>
     					</g:if>
				 		
				 	</ul></div>
				</div>
			</div>
		</nav>
		</div>
		
		<script>
		$( document ).ready(function() {
		    console.log( "ready!" );
		});
		</script>
	</div>
	
	<div class="container">
	<div class="jumbotron">
		
					<g:layoutBody/>
	</div>			
	</div>
	</body>
</html>
